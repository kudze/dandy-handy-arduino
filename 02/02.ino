#include <Arduino.h>
#include <math.h>
//#include <TM1637Display.h>
#include <OneWire.h>                  // Подключаем библиотеку для взаимодействия с устройствами, работающими на шине и по протоколу 1-Wire
#include <DallasTemperature.h>        // Подключаем библиотеку с функциями для работы с DS18B20 (запросы, считывание и преобразование возвращаемых данных)
#include <SoftwareSerial.h>

//Visų naudojamų pinų definicijos
//#define CLK_PIN 2
//#define DIO_PIN 3
#define TEMP_MATUOKLIS_PIN 6
#define SIM900_PIN_TXD 7
#define SIM900_PIN_RXD 8
#define RELE_1_PIN 11
#define RELE_2_PIN 12

//Telefonų numerių definicijos.
#define DOVILES_TEL_NR "+37061518339"
#define DAINIAUS_TEL_NR "+37067834848"

//Sukuriame Ekrano objektą.
//TM1637Display display(CLK_PIN, DIO_PIN);

/*const uint8_t SEG_CCCC[] = {
  SEG_A | SEG_F | SEG_E | SEG_D,                  // C
};

const uint8_t SEG_MINUSAS[] = {
  SEG_G,                  // -
};*/

//Sukuriame temperatūros sensoriaus komunikacijos protokolo objektą.
OneWire oneWire(TEMP_MATUOKLIS_PIN);        // Сообщаем библиотеке об устройстве, работающем по протоколу 1-Wire

//Sukuriame temperatūros sensoriaus objektą.
DallasTemperature sensors(&oneWire);  // Связываем функции библиотеки DallasTemperature с нашим 1-Wire устройством (DS18B20)

//Sukuriame SIM900 modulio objektą.
SoftwareSerial SIM900(SIM900_PIN_TXD, SIM900_PIN_RXD);

//Reikšmės, atitinkančios RELE_1_PIN ir RELE_2_PIN pinų veikią. (false - išjungta, true - įjungta)
bool RELE_1_STATUS = false;
bool RELE_2_STATUS = false;

//Some function defininitions

//Funkcijos deklaracija, vėliau aprašyta apačioje.
void handleSMSInput(String phoneNumber, String sms);

//Nuskaito sensoriaus temperatūrą.
float temperature() { // Измеряем температуру 10 секунд
  sensors.requestTemperatures();                 // Запрос на измерение температуры (повторный)
  float t = float(sensors.getTempCByIndex(0));
  // Получаем значение температуры
  return(t);                                     // Возвращаем значение температуры в место вызова функции
}

/*
//Paskutinė išmatuota temperatūra.
int temperatura = 0;

//Atnaujina ekraną ir iš naujo išmatuoja temperatūrą
void tickDisplay() {
  temperatura = (int) round(temperature()); // ;

  display.setBrightness(0x0f); 
  uint8_t data[] = { 0x0, 0x0, 0x0, 0x0 };
  display.setSegments(data);

  if (temperatura > 0) {
     display.showNumberDec(temperatura, false);
  } else {
     display.setSegments(SEG_CCCC,1,0);
     display.showNumberDec(abs(temperatura), false);
     display.setSegments(SEG_MINUSAS,2,0);
  }
}
*/

//Paskutinė nuskaityta SIM900 atsakymo eilutė
char buffer[100] = {};

//Parodo, ar kita žinutė parodys SMS turinį
bool isNextMessageContent = false;

//Atsimena paskutinį telefono numerį, kuris parašė SIM900 moduliui.
String lastPhoneNumber = "";

//Kiekviena SIM900 modulio eilutė, praeina pro čia ir čia yra sugalvojama ką daryti su ja.
void handleBufferLine() {

  //Mūsų nedomina tuščios SIM900 modulio eilutės
  if(buffer[0] != (char)0) {

    //Pasiverčiame char masyvą į String objektą.
    String buf(buffer);
    //Serial.println("SIM900 responded: " + buf);

    //Jeigu šita eilutė yra SMS turinys, nuskaitę šitą žinutę, turėsime visą informaciją apie ją ir galėsime perduoti kitai funkcijai.
    if(isNextMessageContent) {
      handleSMSInput(lastPhoneNumber, buf);

      //Atresetinam kinamuosius, kad galėtumėme nuskaityti sekančią žinutę
      isNextMessageContent = false;
      lastPhoneNumber = "";
    }

    //Iš visų SIM900 modulio žinučių, tik SMS gavimo pranešimas prasideda +CMT: " raidėmis.
    //Čia realiai galbūt reikėjo normaliai perskaityt ir ištrint žinutę su AT komandomis, nes kažkada SIM korteleje pasibaigs vieta, bet kol kas bus gerai. 
    else if(buf.startsWith("+CMT: \"")) {
      //Pranešame kad kitoje eilutėje bus žinutės turinys.
      isNextMessageContent = true;

      //Kadangi telefono numerio ilgis yra konstanta galime išsitraukti jį žinodami jo pradžios ir pabaigos poziciją.
      lastPhoneNumber = buf.substring(7, 19);
    }
  }

}

//Išvalo paskutinės eilutės bufferį.
void clearBuffer() {
  memset(buffer, 0, sizeof(buffer));
}

//Pabandome nuskaityti SIM900 modulio išvestį
void processInput() {

  if(SIM900.available()) {    
    clearBuffer();

    //Nurodo kurioje bufferio vietoje rašysime
    unsigned int currentBufferPos = 0;

    //Norime leisti šį ciklą kol yra ką nuskaityti.
    while(SIM900.available()) {

        //Nuskaitom raidę
        char c = SIM900.read();

        //\n raidė yra paskutinė eilutėje todėl, po šios raidės išsiunčiame bufferį į parserį.
        if(c == '\n') {

          //Visi stringai turi pasibaigti null raide. (Kitaip imetus į Serial.print galime patekti į Undefined Behaviour statusą)
          buffer[currentBufferPos] = (char)0;

          //Pajungiame parserį
          handleBufferLine();

          //Išvalome bufferį
          clearBuffer();

          //Vel nurodome kad rašytų į bufferio pradžią
          currentBufferPos = 0;
        }

        //Mes norime rašyti visas raides į bufferį išskyrūs priešpaskutinę \r raidę (Dėl šios raidės mes knisomės ilgai.....)
        else if(c != '\r') {

          //Irašome raidę į bufferį.
          buffer[currentBufferPos] = c;

          //Nurodome kad kitą raidę rašytų į sekančią bufferio vietą.
          currentBufferPos++;
        }
    }

    //Ciklui pasibaigus bufferyje liks dar 1 neišparsinta eilutė todėl mes norime ją išparsinti
    //(Duomenu resetinti nereikia, nes funkcijai pasileidus kitą kartą bufferis atsiresetins pats, o currentBufferPos yra šiai funkcijai priklausantis kintamasis)
    handleBufferLine();
  }
}

//Čia funkcija su kuria duodame inputą į SIM900 modulį.
//Parašyta tik dėl to kad ankščiau printinau šias žinutes.
//Dabar kaip ir useless bet delay šitoj vietoj gerai tinka
//Tai kaip ir paliksiu
void SIMprintln(String message) {

  //Parašome į SIM900
  SIM900.println(message); 
  //Serial.println("Sending to SIM: " + message);
  
  //Palaukiame 200ms kad modulis spėtų susiprasti kas ką tik įvyko.
  delay(200);

  //Norime pabandyti paskaityti ar modulis turi ką nors atsakyti po kiekvieno mūsų įvedimo.
  processInput();
}

//Ši komanda išsiunčia SMS žinutę.
void sendSMS(String phoneNumber, String message) {

  //Nustatome SIM900 modulį į SMS modą
  SIMprintln("AT+CMGF=1\r");

  //Pranešame kuriam numeriui siusime žinutę
  SIMprintln("AT + CMGS = \"" + phoneNumber + "\"");

  //Parašome žinutės turinį
  SIMprintln(message); 

  //Po žinutės turinio turi eit CTRL + Z mygtukų kombinacija kad SIM900 modulis suprastu jog tai yra žinutės turinio pabaiga.
  SIMprintln(String((char)26)); 

  //Net nežinau kodėl čia yra tuščia eilutė, bet nesigilinsiu, nes neturiu įrangos šale o siuntinėt daug laiko užima.
  SIMprintln("");

  //Tipo laukiam kol išsius žinutę, galbut ir galima pamažint šitą intevalą bet sueis.
  delay(5000); 
}

//Va čia mes handliname SMS komandas.
void handleSMSInput(String phoneNumber, String sms) {

  //Patikriname ar siuntėjai gali daryti šituos veiksmus.
  if(phoneNumber.equals(DOVILES_TEL_NR) || phoneNumber.equals(DAINIAUS_TEL_NR))
  {
    //bool success = true;
    
    //Konvertuojame SMS žinutę į mažasias raides.
    sms.toLowerCase();

    //Serial.println("handleSMSInput with: " + sms + "!");

    //Galbut čia ir praverstų pilna komandų sistema su argumentų nuskaitymu, bet nedaug komandų čia yra tai ihardocinu tiesiog jaučiu.
    //Tiesiog komandų nuskaitymas ir veiksmų padarymas, nėra čia ką komentuoti manau, viskas savaime paaiškinama.
    if(sms.equals("temp?"))
      sendSMS(phoneNumber, "Temperaturos matuoklis rodo: " + String(temperature()) + "C");

    //Nu čia galim dar pasakyt kad ignoruojam tas žinutes kai RELE_1 yra jau įjungta.
    else if(sms.equals("rele1 on") && !RELE_1_STATUS) {
       digitalWrite(RELE_1_PIN, HIGH);
       RELE_1_STATUS = true;

       sendSMS(phoneNumber, "rele1 on ok");
    }
    
    else if(sms.equals("rele1 off") && RELE_1_STATUS) {
        digitalWrite(RELE_1_PIN, LOW);
        RELE_1_STATUS = false;

        sendSMS(phoneNumber, "rele1 off ok");
    }
    
    else if(sms.equals("rele2 on") && !RELE_2_STATUS) {
        digitalWrite(RELE_2_PIN, HIGH);
        RELE_2_STATUS = true;

        sendSMS(phoneNumber, "rele2 on ok");
    }
    
    else if(sms.equals("rele2 off")&& RELE_2_STATUS) {
        digitalWrite(RELE_2_PIN, LOW);
        RELE_2_STATUS = false;

        sendSMS(phoneNumber, "rele2 off ok");
    }

    else if(sms.equals("status?"))
        sendSMS(phoneNumber, String(RELE_1_STATUS ? "rele1 on" : "rele1 off") + "\n" + String(RELE_2_STATUS ? "rele2 on" : "rele2 off"));
    
    //else {
    //  Serial.println("Užklausa iš atpažinto tel. nr. su neatpažinta komanda: " + sms);
    //  success = false;
    //}
    

    //if(success)
    //  Serial.println("Užklausa iš atpažinto tel. nr. su atpažinta komanda");
  }

  //else
  //  Serial.println("Užklausa iš neatpažino tel. nr.: " + phoneNumber);
  
}

//Main Arduino functions
void setup(void)
{
  Serial.begin(9600);             // Запускаем вывод данных на серийный порт

  //Temperaturos sensorius ijungiam.
  sensors.begin();                // Запускаем библиотеку измерения температуры

  //Užregistruojam RELES pinus.
  pinMode(RELE_1_PIN, OUTPUT);
  pinMode(RELE_2_PIN, OUTPUT);

  //Čia jaučiu šitie nera butini, gal išpradžiu pagal defaultą ir LOW buna šitie pinai, bet atsarga gedos nedaro.
  digitalWrite(RELE_1_PIN, LOW);
  digitalWrite(RELE_2_PIN, LOW);

  //Pajungiam SIM900 modulį
  SIM900.begin(9600);
  //Serial.println("Duodame siek tiek laiko SIM900 moduliui prisijungti prie tinklo");
  //Serial.println("Lauksime 20 sekundziu");
  delay(20000);

  //Čia tipo patikrina ar modulis bum bum ar ne, Bet jeigu jis ne bum bum tai programa net nesuprastu.
  //(Dėl bendro vaizdo sakykim xdd) (Šiaip uždėjau kai su Serial tikrinom viską)
  SIMprintln("AT\r"); 

  //Į sms modą nustatom modulį.
  SIMprintln("AT+CMGF=1\r"); 

  //Čia kažkas su notificationais kiek pamenu.
  //Nebepamenu ką daro tbh.
  //Bet be jo atsimenu neveikė sms išsiuntimas.
  SIMprintln("AT+CNMI=2,2,0,0,0\r");

  //Nustatom miegojimo tipą, kad galetumeme prikelt per serial jungti su sim900.
  //Actually gal ir 1 miegojimo tipas turėtu veikt bet a druk reikės ji pakelt.
  //Nors kol kas mes jo nekeliam, tik SMS prikelt ji turetu.
  //Turetu veikt po 5 sekundžiu neaktyvumo.
  SIMprintln("AT+CSCLK=2\r");
}

uint8_t a = 0;
void loop(void)
{
  if(a != 5) {
    a++;
  }

  else {
    Serial.println("SIM900 modulis tokiu laiku turėtų užmigt.");
  }
  
  //Atnaujinam ekraniuką ir nuskaitom temp.
  //tickDisplay();

  //Žiurim ar kažkas parašė mums
  processInput();

  //Miegam sekundę.
  delay(1000);
}
