
#include <OneWire.h>
#include <DallasTemperature.h>

//Čia turi būti pino numeris, prie kurio prikabintas DS18B20 DQ laidas. (Data Input/Output)
#define TEMPERATURE_PIN 2

OneWire oneWireConnection(TEMPERATURE_PIN);

DallasTemperature sensors(&oneWireConnection);

void setup() {
  Serial.begin(9600);
  
  sensors.begin();
}

void loop() {

    Serial.print("DS18B20 gaudoma temperatūra: ");
    Serial.println(requestTemp());
    
    delay(1000);

}

//Gražina DS18B20 rodomą temperatūrą celcijumi
float requestTemp() {
  sensors.requestTemperatures();

  return sensors.getTempCByIndex(0);
}
